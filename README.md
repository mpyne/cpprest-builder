# C++ Build Host

This is the source for a simplistic Alpine Linux-based C++ development
container.  It is intended to act as a reproducible build setup for separate
C++-based software to become very small Docker containers (static build and
hosted from the "scratch" repository).

# Running

You don't run this. Instead you build *other* Docker containers using the
multi-stage build concept, to load your source code into a running container
with this build environment, build your static binary, and then copy the binary
into the container image you're actually trying to create.

See the "Sample Navy PRT API Server" for an example.

# Building

To build the container, it is sufficient to run `docker build` as normal, as
demonstrated in the `build-image` script.

The custom steps are included in the `build-steps` script, which is run by
default as part of the build once the Alpine Linux-hosted apk packages are
installed.

Currently this script installs the "Served" C++ REST SDK along with required
dependencies of that library.

# Author

Michael Pyne <michael.pyne@gmail.com>. Do note that this is as much for
self-learning on how to use Docker as it is for a sample API so don't go
copying this blindly.
