FROM alpine:3.11

# Contains build steps for git-based C++ modules
COPY build-steps /tmp

# Setup build environment and key dependencies
RUN apk update && apk --no-cache add \
    build-base \
    boost-dev \
    cmake \
    git \
&& sh ./tmp/build-steps
